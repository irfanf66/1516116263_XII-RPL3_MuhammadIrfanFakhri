<?php

use Illuminate\Database\Seeder;

class accountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User;
        $user->name = 'Admin';
        $user->email = 'm.irfan.fakhri66@gmail.com';
        $user->password = bcrypt('admin');
        $user->level = '3';
        $user->save();
    }
}
