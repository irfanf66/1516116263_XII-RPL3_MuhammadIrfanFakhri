<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KelasController extends Controller
{
    public function __construct($value='')
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	return view('kelas.index', [
    		'kelas' => \App\Kelas::all()
    		]);
    }

    public function create()
    {
    	return view('kelas.form');
    }

    public function store(Request $request)
    {
    	$input = $request->all();
    	$this->validate($request, $this->rules());

    	if (\App\Kelas::create($input)) {
        	return redirect('/')->with('success', 'Data berhasil ditambahkan');
        }
        
        return redirect('/')->with('error', 'Data gagal ditambahkan');
    }

    public function edit($id='')
    {
        return view('kelas.form', [
            'data' => \App\Kelas::where('id_kelas', $id)->first(),
            ]);
    }

    public function update($id='', Request $request)
    {
        $input = $request->all();
        $this->validate($request, $this->rules());
        if (\App\Kelas::where('id_kelas', $id)->update([
            'nama_kelas' => $input['nama_kelas'],
            'jurusan' => $input['jurusan']
            ])) {
            return redirect('/')->with('success', 'Data berhasil diubah');
        }
        return redirect('/')->with('error', 'Data gagal diubah');
    }

    public function destroy($id='')
    {
        if (\App\Kelas::where('id_kelas', $id)->delete()) {
            return redirect('/')->with('success', 'Data berhasil dihapus');
        }
        return redirect('/')->with('error', 'Data gagal dihapus');
    }

    public function rules()
    {
    	return [
    		'nama_kelas' 	=> 'required|max:100',
    		'jurusan'		=> 'required|max:100'
    	];
    }
}
