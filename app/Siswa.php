<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 't_siswa';
    protected $primaryKey = 'nis';
    protected $fillable = [
    	'nis', 'nama_lengkap', 'jenis_kelamin', 'alamat', 'no_telp', 'id_kelas', 'foto',
    ];

    public function getJenisKelaminAttribute()
    {
    	if (@$this->attributes['jenis_kelamin'] == 'L')
    		return 'Laki - Laki';
    	if (@$this->attributes['jenis_kelamin'] == 'P')
    		return 'Perempuan';
    	return '';
    }

    public function kelas()
    {
    	return $this->hasOne('\App\Kelas', 'id_kelas', 'id_kelas');
    }
}
