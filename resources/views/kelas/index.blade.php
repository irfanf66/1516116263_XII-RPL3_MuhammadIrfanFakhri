@extends('layouts.app')
@section('content')

	<section class="content-header">
      <h1>
        Data Kelas
        <small>SMK Negeri 4 Bandung</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Kelas</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    	@include('layouts.feedback')
    	<div class="box">
    		<div class="box-header with-border">
    			<a href="{{ url('kelas/add') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
    		</div>
    		<div class="box-body">
    			
			    <table class="table table-stripped">
			    	<thead>
			    		<tr>
			    			<th>No</th>
			    			<th>Nama Kelas</th>
			    			<th>Jurusan</th>
			    			<th>Action</th>
			    		</tr>
			    	</thead>
			    	<tbody>
			    		@foreach ($kelas as $val)
			    		<tr>
			    			<td>{{ (!empty($i)) ? ++$i : $i = 1 }}</td>
			    			<td>{{ $val->nama_kelas }}</td>
			    			<td>{{ $val->jurusan }}</td>
			    			<td>
			    				<a href="{{ url('kelas/' . $val->id_kelas . '/edit') }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
			    				<form action="{{ url('kelas/' . $val->id_kelas . '/delete') }}" style="display: inline;">
			    					{{ csrf_field() }}
				    				<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
			    				</form>
			    			</td>
			    		</tr>
			    		@endforeach
			    	</tbody>
			    </table>

    		</div>
    	</div>

	 </section>

@endsection